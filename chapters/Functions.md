# Функции


Мы ознакомились с вложенными функциями, однако, ещё плохо знаем что они из себя представляют. 
Это пример примитивной функции, которая находится в модуле, 
и находится в пространстве имён `Helpers`.

```fsharp
    namespace Helpers

    module Maths =
        let add num1 num2 =
            num1 + num2
```

Рассмотрим код построчно:

* `namespace Helpers` сообщает, что модуль требуется сопоставить с пространством имён _Helpers_.

* `module Maths =` создаёт модуль _Maths_. F# чувствителен к отступам, за счёт чего уменьшается количество скобок.

* `let add num1 num2 =` объявляет функцию. Объявление любых переменных в F# начинается с ключевого слова **let**. В F# переменные неизменяемые или иммутабельные, даже если используется var. Ещё для вас может оказаться ( The next strange thing you will have noticed with this line is that there appears to be three values to the left of the equals sign). Это одна функция. Первое значение — название функции (_add_), далее, второе и третье значение — это первый (_num1_) и второй (_num2_) аргумент функции соотвественно.

* Тело функции _add_ `num1 + num2`. Здесь нет привычного как в C# оператора _return_, так как все 
функции являются выражениями. В C# есть эквивалентная стуктура -- лямбда-выражения, однако, в F# фигурные скобки не нужны.

Рассмотрим стартовый проект консольного F# приложения:

```fsharp
[<EntryPoint>]
let main argv =
    printfn "%A" argv
    0 // успешное завершение работы программы
```

`[<EntryPoint>]` -- это атрибут. Синтаксис немного отличается от C#, так как в
последнем есть только квадратные скобки. Данный атрибут указывает на начальный
(Main) метод входную точку. Входная точка в F# важна по двум причинам:

* Данный файл не модуль и не пространство имён (namespace)
* Данный файл долженее быть скомпилирован самым последним.

`let main argv =` это функция, которая принимает аргументы при запуске программы
как параметр.

`printfn "%A" argv` выводит аргументы программы в консоль. `printfn` похож на
Console.WriteLine с String.Format. `%A` выводит тип значения. Информацию о
токенах форматирования в функции `printf` можете найти [здесь][1]. We can also
see the syntax for calling a function in F# on this line. Much like function
declarations, parameters are separated by spaces with no need for commas and
parenthesis. This line is almost equivalent to 
`Console.WriteLine(String.Format("{0}", argv))`.

`0` - это возвращаемое функцией значение. Так как у функции атрибут EntryPoint,
то это значит что она возвращает exit-code.

Функции могут содержать собственные вложенные функции. Как правило, это
используется для сокрытия методов, установки модификаторов `protected`, `public`
или `private` как в C#.

```fsharp
let printSignedIn username  =
    let print m =
        Console.WriteLine(DateTime.Now.ToShortDateString() + " " + m)
    print "Welcome"
    print ("You have successfully as " + username)
```

В примере видно, что функция `print` доступна только внутри функции
`printSignedIn`.

[1]:
https://msdn.microsoft.com/en-us/visualfsharpdocs/conceptual/core.printf-module-[fsharp]#remarks
